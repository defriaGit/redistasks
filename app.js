var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyparser = require('body-parser');
var redis = require('redis');

var app = express();

// Create client
var client = redis.createClient(6379, '192.168.1.111');

client.on('connect', function () {
    console.log('Redis server connected.')
})


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res) {
    let title = 'Task List';

    client.lrange('tasks', 0, -1, function (err, reply) {
        client.hgetall('call', function (err, call) {
            res.render('index', {
                title: title,
                tasks: reply,
                call: call
            });
        });
    });
});

app.post('/task/add', function (req, res) {
    let task = req.body.task;
    client.rpush('tasks', task, function (err, reply) {
        if (err) {
            console.log(err);
        }
        console.log('Task added');
        res.redirect('/');
    });
});

app.post('/task/delete', function (req, res) {
    let tasksToDelete = req.body.tasks;
    client.lrange('tasks', 0, -1, function (err, tasks) {
        for (var i = 0; i < tasks.length; i++) {
            if (tasksToDelete.indexOf(tasks[i]) > -1) {
                client.lrem('tasks', 0, tasks[i], function () {
                    if (err) {
                        console.log(err);
                    }
                });
            }
        }
        res.redirect('/');
    });
});

app.post('/call/add', function (req, res) {
    let newCall = {};

    newCall.name = req.body.name;
    newCall.company = req.body.company;
    newCall.phone = req.body.phone;
    newCall.time = req.body.time;

    client.hmset('call', ['name', newCall.name, 'company', newCall.company, 'phone', newCall.phone, 'time', newCall.time], function (err, reply) {
        if (err) {
            console.log(err);
        }
        console.log(reply);
        res.redirect('/');
    });
});

let port = 3001;
app.listen(port);
console.log('Server started on port ', port);

module.exports = app;

